#!/bin/bash

INPUT="./"

function total_files {
    find . -type f | wc -l
}

echo -n "Files:"
total_files $INPUT
